#!/usr/bin/python3
import argparse
from base64 import b64encode
from io import StringIO
import json
import os
import socket
import ssl
from subprocess import run, STDOUT, PIPE
import sys
import time
from urllib.request import Request, urlopen, HTTPError

BORG = '/usr/bin/borg'
DEFAULT_FLAGS = [
    '--compression', 'lz4'
]


class ScriptError(Exception):
    pass


class Logger(StringIO):
    def __init__(self, verbose=False):
        super(Logger, self).__init__()
        self.verbose = verbose

    def write(self, s):
        super(Logger, self).write(s)
        if self.verbose:
            sys.stdout.write(s)
            sys.stdout.flush()


class Backup(object):
    def __init__(self, conf_file, args):
        try:
            with open(os.path.expanduser(conf_file), 'r') as fp:
                conf = json.load(fp)
        except Exception as e:
            raise ScriptError(
                'Unable to read configuration file ({})'.format(e))

        if 'sources' not in conf:
            raise ScriptError('No "sources" in configuration file')

        if 'dest' not in conf:
            raise ScriptError('No "dest" in configuration file')

        conf.setdefault('pre_commands', [])
        conf.setdefault('post_commands', [])
        conf.setdefault('exclude', [])
        conf.setdefault('env', {})
        conf.setdefault('flags', list(DEFAULT_FLAGS))
        conf.setdefault('archive', '::{now}')
        conf.setdefault('prune', None)

        self.conf = conf
        self.args = args
        self.logger = Logger(not self.args.quiet)
        self.duration = 0

    def log(self, s):
        print(s, file=self.logger)

    def test_hosts(self):
        for x in self.conf.get('check_hosts', []):
            x = x.format(**self.conf)
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(5)
            try:
                sock.connect((x, 22))
            except Exception as e:
                raise ScriptError(
                    'host {} is unreachable ({})'.format(x, e))

    def _log_env(self, env):
        for k, v in sorted(env.items()):
            if k == 'BORG_PASSPHRASE':
                v = '*******'
            self.log('{}={}'.format(k, v))

    def _run_cmd(self, cmd, **kwargs):
        kwargs.setdefault('shell', False)
        if self.args.quiet:
            kwargs['stderr'] = STDOUT
            kwargs['stdout'] = PIPE

        p = run(cmd, **kwargs)

        if self.args.quiet:
            self.log(p.stdout.decode('utf8').strip())

        self.log('> {}'.format(p.returncode))

        return p

    def _exec_commands(self, commands, print_only=False):
        returns = []
        for cmd in commands:
            cmd = cmd.format(**self.conf)

            p = self._exec_command(cmd, print_only=print_only, shell=True)
            if p is not None:
                returns.append(p.returncode)

        if any(x for x in returns if x != 0):
            return 1
        return 0

    def _exec_command(self, cmd, print_only=False, **kwargs):
        self.log('\n== Executing: {}\n'.format(
            cmd if isinstance(cmd, str) else ' '.join(cmd)
        ))
        if self.args.print_only or print_only:
            return

        return self._run_cmd(cmd, **kwargs)

    def _build_command(self):
        conf = self.conf
        cmd = [BORG, 'create']

        cmd.extend(conf['flags'])
        cmd.append(conf['dest'] + conf['archive'])
        cmd.extend(conf['sources'])

        if self.args.verbose:
            cmd.insert(2, '--list')

        if self.args.dryrun:
            cmd.insert(2, '--dry-run')
        else:
            cmd.insert(2, '--stats')

        return cmd

    def _check_repo(self, env):
        cmd = [BORG, 'info', self.conf['dest']]
        p = self._exec_command(cmd, env=env)
        if p is None:
            return

        if p.returncode == 2:
            return False

        if p.returncode != 0:
            raise ScriptError('Error running borg info')

        return True

    def _init_repo(self, env):
        cmd = [BORG, 'init', '-e', 'repokey-blake2', self.conf['dest']]
        p = self._exec_command(cmd, print_only=self.args.dryrun, env=env)
        if p.returncode != 0:
            raise ScriptError('Error running borg init')

    def _prune_repo(self, env):
        cmd = [BORG, 'prune', '--stats', '--list']
        if self.args.dryrun:
            cmd.append('--dry-run')

        for k, v in self.conf['prune'].items():
            cmd.extend(['--keep-{}'.format(k), str(v)])

        p = self._exec_command(cmd, env=env)
        if p is not None and p.returncode != 0:
            raise ScriptError('Error running borg prune')

    def _run(self):
        try:
            self.test_hosts()
        except Exception as e:
            self.log('ERROR: {}'.format(e))

        # Execute pre_commands
        if self._exec_commands(self.conf['pre_commands'],
                               print_only=self.args.dryrun) != 0:
            return 1

        # Execute borg command
        env = dict(os.environ)
        env.update(self.conf['env'])

        self.log('\n== Env')
        self._log_env(env)

        if self._check_repo(env) is False:
            self._init_repo(env)

        cmd = self._build_command()
        self.log('\n== Executing: {}'.format(' '.join(cmd)))
        if not self.args.print_only:
            p = self._run_cmd(cmd, env=env)
            if p.returncode != 0:
                return p.returncode

        if self.conf['prune']:
            self._prune_repo(env)

        # Execute post_commands
        if self._exec_commands(self.conf['post_commands'],
                               print_only=self.args.dryrun) != 0:
            return 1
        return 0

    def _apply_tpl(self, o, vars):
        if isinstance(o, str):
            o = o.format(o, **vars)
            if o.isdigit():
                o = int(o)
        elif isinstance(o, dict):
            for k in o:
                o[k] = self._apply_tpl(o[k], vars)
        elif isinstance(o, (list, tuple)):
            o = [self._apply_tpl(x, vars) for x in o]

        return o

    def _http_notify(self, return_code):
        params = self.conf['http_notify']
        url = params['url']
        method = params.get('method', 'GET').upper()
        headers = params.get('headers', {})
        if 'auth' in params:
            auth = b64encode(params['auth'].encode('utf8')).decode('ascii')
            headers['Authorization'] = 'Basic {}'.format(auth)

        r = Request(url, headers=headers, method=method)

        data = None
        out = 'Backup finished: {}\n\n{}'.format(
            return_code,
            self.logger.getvalue().strip()
        )
        if 'json' in params:
            vars = {
                'status': return_code,
                'out': out,
                'nagios_status': 0 if return_code == 0 else 2,
                'duration': round(self.duration, 2),
            }
            data = self._apply_tpl(params['json'], vars)
            data = json.dumps(data, ensure_ascii=True).encode('utf8')

        args = {'data': data, 'timeout': 5}
        if params.get('ssl_check', True) is False:
            args['context'] = ssl.create_default_context()
            args['context'].check_hostname = False
            args['context'].verify_mode = ssl.CERT_NONE

        try:
            urlopen(r, **args)
        except HTTPError as e:
            raise ScriptError('HTTP Error: {}'.format(e))

    def run(self):
        start = time.time()
        r = self._run()
        self.duration = time.time() - start

        if not self.args.dryrun and not self.args.print_only \
                and 'http_notify' in self.conf:
            self._http_notify(r)

        return r


def main(args):
    try:
        b = Backup(args.conf_file, args)
    except Exception as e:
        sys.stderr.write('ERROR: {} - (conf: {})\n'.format(e, args.conf_file))
        sys.exit(1)

    try:
        ret = b.run()
    except Exception as e:
        sys.stderr.write('ERROR: {} - (conf: {})\n'.format(e, args.conf_file))
        sys.exit(2)

    sys.exit(ret)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Backup files to a borg target'
    )
    parser.add_argument(
        'conf_file', metavar='config',
        help='Configuration file'
    )
    parser.add_argument(
        '-v', '--verbose',
        dest='verbose', action='store_true',
        help='Run rsync commands with --verbose flag')
    parser.add_argument(
        '-q', '--quiet',
        dest='quiet', action='store_true', default=False,
        help='Do not print anything on the output')
    parser.add_argument(
        '-p', '--print',
        dest='print_only', action='store_true',
        help='Only print executed commands on output')
    parser.add_argument(
        '-s', '--dry-run',
        dest='dryrun', action='store_true',
        help='Run rsync commands with --dry-run flag')

    args = parser.parse_args()

    main(args)
