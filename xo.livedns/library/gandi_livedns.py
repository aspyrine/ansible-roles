#!/usr/bin/python
from collections import OrderedDict
import json
from operator import itemgetter

from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import open_url, urllib_request

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: gandi_livedns

short_description: Manage Gandi LiveDNS domain records

version_added: "2.8"

description:
    This module takes a list of domain records and update
    the domain's records on Gandi LiveDNS service.

options:
    api_key:
        description:
            - Gandi API key (v5)
        required: true
    domain:
        description:
            - Domain name
        required: true
    records:
        description:
            - List of records. Each record is a colleciton of
              "type", "name", "values" and (optional) "ttl"

author:
    - Olivier Meunier
'''


class Record(OrderedDict):
    @classmethod
    def from_params(cls, params, defaults):
        return cls((
            ('rrset_name', params['name']),
            ('rrset_type', params['type']),
            ('rrset_ttl', params['ttl'] or defaults['ttl']),
            ('rrset_values', isinstance(params['values'], (list, tuple))
             and params['values'] or [params['values']])
        ))

    @classmethod
    def from_api(cls, record):
        return cls((
            ('rrset_name', record['rrset_name']),
            ('rrset_type', record['rrset_type']),
            ('rrset_ttl', record['rrset_ttl']),
            ('rrset_values', record['rrset_values'])
        ))


def api_error(rsp):
    ret = {'status': rsp.code, 'body': rsp.read()}

    if rsp.headers.get('content-type', '').split(';')[0] == 'application/json':
        ret['body'] = json.loads(ret['body'])

    return ret


def run_module():
    module = AnsibleModule(
        argument_spec=dict(
            api_key=dict(type='str', required=True, no_log=True),
            domain=dict(type='str', required=True),
            default_ttl=dict(type='int', default=18000),
            keep_records=dict(
                type='list',
                elements='dict',
                options=dict(
                    name=dict(type='str', required=True),
                    type=dict(type='str', required=True),
                ),
                default=[],
            ),
            records=dict(
                type='list',
                required=True,
                elements='dict',
                options=dict(
                    name=dict(type='str', required=True),
                    type=dict(type='str', required=True),
                    ttl=dict(type='int'),
                    values=dict(type='list', elements='str')
                )
            ),
        ),
        supports_check_mode=True
    )

    api_key = module.params.get('api_key')
    domain = module.params.get('domain')

    defaults = {'ttl': module.params.get('default_ttl')}
    records = [Record.from_params(x, defaults)
               for x in module.params.get('records')]

    url = 'https://dns.api.gandi.net/api/v5/domains/{}/records'.format(domain)
    headers = {'X-Api-Key': api_key, 'Content-Type': 'application/json'}
    try:
        r = open_url(url, headers=headers)
    except urllib_request.HTTPError as e:
        module.fail_json(msg=api_error(e))

    current_records = [Record.from_api(x) for x in json.loads(r.read())]
    current_records.sort(key=itemgetter('rrset_name', 'rrset_type'))

    # Add kept records to list
    keep_records = [(x['name'], x['type'])
                    for x in module.params.get('keep_records')]
    for x in current_records:
        if (x['rrset_name'], x['rrset_type']) in keep_records:
            records.append(x)

    records.sort(key=itemgetter('rrset_name', 'rrset_type'))

    changed = current_records != records

    if not changed:
        module.exit_json(changed=False)

    result = dict(
        changed=True,
        diff={
            'before': json.dumps(current_records, indent=2),
            'after': json.dumps(records, indent=2)
        }
    )

    if module.check_mode:
        module.exit_json(**result)

    # Update records now
    try:
        r = open_url(url, headers=headers, method='PUT',
                     data=json.dumps({'items': records}))
    except urllib_request.HTTPError as e:
        module.fail_json(msg=api_error(e))

    module.exit_json(**result)


if __name__ == '__main__':
    run_module()
