#!/bin/bash

set -u

if [ -z ${GANDI_API_KEY+x} ]; then
    echo "UNKNOWN - GANDI_API_KEY is not set"
    exit 3
fi

check_domain () {
    local domain=$1
    local critical=$2
    local warning=$3

    local response=$(curl --connect-timeout 10 --max-time 10 \
      --silent --write-out "HTTPSTATUS:%{http_code}" \
      --request GET \
      --url https://api.gandi.net/v5/domain/domains/${domain} \
      --header "Authorization: apikey ${GANDI_API_KEY}")

    local body=$(echo ${response} | sed -e 's/HTTPSTATUS\:.*//g')
    local status=$(echo ${response} | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

    if (( ${status} / 100 != 2)); then
        echo "UNKNOWN - Invalid HTTP status code ${status}"
        return 3
    fi

    local expires=$(echo ${body} | jq -r ".dates.registry_ends_at")

    local expires_ts=$(date -d ${expires} +%s)
    local limit_ts=$(date +%s)
    local delta=$(( (($expires_ts) - ($limit_ts)) / 86400 ))

    local line="Domain will expire in ${delta} days (${expires}) | domain_days_until_expiry=${delta};${warning};${critical}"

    if [ ${delta} -le ${critical} ]; then
        echo "CRITICAL - ${line}"
        return 2
    elif [ ${delta} -le ${warning} ]; then
        echo "WARNING - ${line}"
        return 1
    fi

    echo "OK - ${line}"
    return
}


short_opts='d:c:w:'
long_opts='domain:,critical:,warning:'
args=$(getopt --shell sh -l "$long_opts" -o "$short_opts" -- "$@")
if [ $? -ne 0 ]; then
    echo "Type ' --help' to get usage information."
    exit 1
fi
eval set -- $args

critical=7
warning=30

while [ $# -gt 0 ]; do
    case "$1" in
        --domain|-d) domain="$2"; shift ;;
        --critical|-c) critical="$2"; shift ;;
        --warning|-w) warning="$2"; shift ;;
        --)           shift; break ;;
    esac
    shift
done

if [ -z ${domain+x} ]; then
    echo "UNKNOWN - --domain/-d is not set"
    exit 3
fi

check_domain ${domain} ${critical} ${warning}
