#!/usr/bin/python3
import argparse
from collections import OrderedDict
from datetime import datetime
from email.utils import parsedate_to_datetime
from ipaddress import ip_network
import json
import logging
import logging.handlers
import os
from pathlib import Path
from urllib.request import Request, urlopen
from subprocess import run, PIPE
import sys

BASE_URL = "https://iplists.firehol.org/files/"
CONFDIR = Path("/etc/iplists")


def setup_logger(name, handler, level, fmt=None):
    logger = logging.getLogger(name=name)
    logger.setLevel(level)
    if fmt is not None:
        formatter = logging.Formatter('%(message)s')
        handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


log = setup_logger(
    'log',
    logging.StreamHandler(sys.stdout), logging.DEBUG,
    '%(message)s')


def download_list(name, src, force=False):
    list_file = (CONFDIR / name).with_suffix(".nft")
    last_mod = None

    log.debug('# %s', name)

    fetag = get_file_etag(list_file)

    rsp = None
    rq = Request(
        url="{}{}".format(BASE_URL, src),
        headers={
            'User-Agent': 'curl/7.74.0',
            'Accept': '*'
        }
    )
    log.debug("++ downloading %s", rq.full_url)

    with urlopen(rq) as rsp:
        etag = rsp.headers["etag"]
        if not force and etag == fetag:
            log.debug("++ file already the most recent version")
            return False
        data = rsp.read()
        try:
            last_mod = parsedate_to_datetime(rsp.headers.get("last-modified"))
        except Exception:
            pass

    values = OrderedDict((ip_network(x.decode("utf8")).__str__(), None)
                         for x in data.splitlines()
                         if not x.startswith(b"#") or x.strip() == b"")

    tmp = "{}~".format(list_file)
    with open(tmp, "w") as fp:
        fp.writelines([
            "# {}\n".format(etag),
            "set iplists_{} ".format(name), "{"
            "\n  typeof ip saddr;"
            "\n  flags interval;",
            "\n  policy memory;",

            "\n  elements = {\n    ",
            ",\n    ".join(values.keys()),
            "\n  }\n",

            "}\n",
        ])

    os.rename(tmp, str(list_file))
    if last_mod:
        md = datetime.timestamp(last_mod)
        os.utime(str(list_file), times=(md, md))

    log.info("%s written", list_file)
    return True


def parse_last_mod(rsp):
    if not rsp.headers.get("last-modified"):
        return

    return parsedate_to_datetime(rsp.headers["last-modified"])


def get_file_etag(list_file):
    if not list_file.exists():
        return None

    with list_file.open("r") as fp:
        return fp.readline().lstrip("# ").rstrip()


if __name__ == "__main__":
    parser = argparse.ArgumentParser('Update iplists lists')
    parser.add_argument('-l', '--list', action='store_true', default=False,
                        help='Show configured lists')
    parser.add_argument('-c', '--cron', action='store_true', default=False,
                        help='Cron mode')
    parser.add_argument('-f', '--force', action='store_true', default=False,
                        help='Force list refresh')

    args = parser.parse_args()
    if args.cron:
        log = setup_logger(
            'iplists-update',
            logging.handlers.SysLogHandler(address='/dev/log'),
            logging.INFO)

    with (CONFDIR / "sources.json").open("r") as fp:
        data = json.load(fp)

    if len(data) == 0:
        sys.exit(0)

    changes = []
    for name, src in sorted(data.items()):
        if args.list:
            print('{}: {}'.format(name, src))
            continue
        try:
            changes.append(download_list(name, src, args.force))
        except Exception as e:
            log.exception(e)
            continue

    changes = [x for x in changes if x]
    if changes:
        log.info('%d list(s) updated', len(changes))
        log.debug('Lists changed restarting nftables')
        p = run(['/bin/systemctl', 'reload', 'nftables.service'],
                stderr=PIPE, stdout=PIPE)
        if p.returncode == 5:
            log.warn((p.stdout + p.stderr).decode('utf8'))
        elif p.returncode != 0:
            sys.stderr.write((p.stdout + p.stderr).decode('utf8'))
            sys.exit(2)
    else:
        log.info('No list change')
