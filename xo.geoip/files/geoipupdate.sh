#!/bin/bash

set -u

function error {
    echo "$0: error: $1" >&2
    exit 1
}

function download_database {
    DEST_FILE="dbip4.dat.gz"
    URL="https://dl.miyuru.lk/geoip/dbip/country/dbip4.dat.gz"

    curl -s -L -o ${DEST_FILE} "${URL}" || error "Failed to download: ${URL}"

    gunzip ${DEST_FILE}
}

function main {
    # Create a temporary directory
    export TEMPDIR=$(mktemp --directory)
    pushd $TEMPDIR > /dev/null 2>&1

    # Download
    download_database

    # Copy database
    mkdir -p /var/lib/geoip
    cp dbip4.dat /var/lib/geoip/

    popd > /dev/null 2>&1

    rm -rf ${TEMPDIR}
}

main
