#!/bin/bash
set -e
set -u

DIST="buster"
DEFAULT_SYS_SIZE=4G
DEFAULT_SWAP_SIZE=1G

LIBVIRT_STORAGE_TYPE=lvm
LIBVIRT_STORAGE_POOL=vg0
LIBVIRT_NET_BRIDGE=brlan

# Read defaults
test -f /etc/default/create-vm && source /etc/default/create-vm

SCRIPTNAME="$(basename $0)"


help() {
    cat <<EOH
Usage: $SCRIPTNAME name [options]

-h, --help      Help
--ip            VM IP address
--dist          Distribution name ($DIST)
EOH
    exit ${1:-0}
}


create_volume() {
    storage_type=$1
    storage_name=$2
    storage_size=$3
    storage_dev=$4

    if [[ -b ${storage_dev} ]]; then
        read -p "Volume ${storage_dev} exists. Overwrite? [y|N] " -n 1 -r
        echo
        if [[ ! $REPLY =~ ^y|Y$ ]]; then
            return 1
        fi
        return 0
    fi

    echo "Creating ${storage_dev}..."
    case "$storage_type" in
        "lvm")
            /sbin/lvcreate --size ${storage_size} -n ${storage_name} ${LIBVIRT_STORAGE_POOL}
            ;;
        "zfs")
            /sbin/zfs create -V ${storage_size} ${LIBVIRT_STORAGE_POOL}/${storage_name}
            ;;
    esac
}



#
# Default variables
#
vm_dist=${DIST}
vm_ip=""

#
# Read options
#
OPTS=$(getopt -n ${SCRIPTNAME} -o h --long dist:,ip:,help -- "$@")
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi
eval set -- "$OPTS"

while true; do
    case "$1" in
        --dist) vm_dist="$2"; shift 2 ;;
        --ip) vm_ip="$2"; shift 2 ;;
        -h|--help) help ;;
        --) shift; break ;;
        *) echo "Error: $1"; exit 1;;
    esac
done

vm_name=${1:-}
if [ "${vm_name}" = "" ]; then
    echo "** Error: No VM name provided\n"
    help 1
fi


if [[ "$LIBVIRT_STORAGE_TYPE" == "zfs" ]]; then
    pool_base="/dev/zvol/${LIBVIRT_STORAGE_POOL}"
else
    pool_base="/dev/${LIBVIRT_STORAGE_POOL}"
fi

vm_vol_sys=x-${vm_name}-sys
vm_vol_swp=x-${vm_name}-swap

vm_vol_sys_dev=${pool_base}/${vm_vol_sys}
vm_vol_swp_dev=${pool_base}/${vm_vol_swp}

vm_vol_sys_size=${DEFAULT_SYS_SIZE}
vm_vol_swp_size=${DEFAULT_SWAP_SIZE}


# Show a summary
cat <<EOF
VM Create summary
Dist:          ${vm_dist}
IP address:    ${vm_ip}
Storage:       ${LIBVIRT_STORAGE_TYPE} / ${LIBVIRT_STORAGE_POOL}
System volume: ${vm_vol_sys_dev} (${vm_vol_sys_size})
Swap volume:   ${vm_vol_swp_dev} (${vm_vol_swp_size})
EOF


# Confirm and ask for a root password
read -p "Start VM creation? [Y|n] " -n 1 -r
echo
if [[ ! $REPLY =~ ^([Yy]|)$ ]]; then
    exit 1
fi

read -p "Root password: " -s root_password
echo


# Create volumes
create_volume ${LIBVIRT_STORAGE_TYPE} $vm_vol_sys $vm_vol_sys_size $vm_vol_sys_dev || exit 1
create_volume ${LIBVIRT_STORAGE_TYPE} $vm_vol_swp $vm_vol_swp_size $vm_vol_swp_dev || exit 1


# Create filesystems
mkfs.ext4 -F ${vm_vol_sys_dev}
e2fsck -f ${vm_vol_sys_dev}

mkswap ${vm_vol_swp_dev}


#
# Create image
#
xen-create-image \
  --verbose \
  --image-dev ${vm_vol_sys_dev} \
  --swap-dev ${vm_vol_swp_dev} \
  --hostname ${vm_name} \
  --dhcp \
  --pygrub \
  --dontformat \
  --nopasswd \
  --dist ${DIST}


#
# Hooks
#

# Mount device
mp=/mnt/xen-tmp
mkdir -p /mnt/xen-tmp
mount ${vm_vol_sys_dev} ${mp}

# Mask tty2 to tty6
chroot ${mp} ln -sf /dev/null /etc/systemd/system/getty@tty2.service
chroot ${mp} ln -sf /dev/null /etc/systemd/system/getty@tty3.service
chroot ${mp} ln -sf /dev/null /etc/systemd/system/getty@tty4.service
chroot ${mp} ln -sf /dev/null /etc/systemd/system/getty@tty5.service
chroot ${mp} ln -sf /dev/null /etc/systemd/system/getty@tty6.service

# Remove some service
chroot ${mp} apt-get remove --purge -q -y isc-dhcp-common

# Install packages
chroot ${mp} apt-get install -q -y ethtool python lsb-release python-apt

# Fix mount options
sed -i 's#^\(/dev/xvda[0-9]\+\s\+/ ext4\s\+\).*#\1 noatime,nodiratime,errors=remount-ro 0 1#' ${mp}/etc/fstab

# Copy ssh authorized keys
mkdir -p ${mp}/root/.ssh
chmod 0700 ${mp}/root/.ssh
cp -p /root/.ssh/authorized_keys ${mp}/root/.ssh/

# Create network interfaces
if [[ "${vm_ip}" != "" ]]; then
    cat <<EOF > ${mp}/etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet static
    address ${vm_ip}
    netmask 255.255.255.0
    gateway 192.168.100.1

EOF
fi

# Set root password
echo "root:${root_password}" | chroot ${mp} chpasswd

# Umount device
umount ${mp}
rmdir ${mp}

rm -f /etc/xen/${vm_name}.cfg

#
# Libvirt installation
#
if (virsh list --all --name | tail -n +1 | grep -E "^${vm_name}$" > /dev/null); then
    echo "VM already exists. All done!"
    exit
fi


mac=$(openssl rand -hex 3 | sed 's/\(..\)/\1:/g; s/.$//')
vm_mac=$(echo "00:16:3e:$mac" | tr '[:lower:]' '[:upper:]')
vm_uuid=$(cat /proc/sys/kernel/random/uuid)

mkdir -p /etc/libvirt/libxl

cat <<EOF > /etc/libvirt/libxl/${vm_name}.xml
<domain type='xen'>
  <name>${vm_name}</name>
  <uuid>${vm_uuid}</uuid>
  <memory unit='KiB'>262144</memory>
  <currentMemory unit='KiB'>262144</currentMemory>
  <vcpu placement='static'>1</vcpu>
  <bootloader>/usr/lib/xen-4.11/bin/pygrub</bootloader>
  <os>
    <type arch='x86_64' machine='xenpv'>linux</type>
    <cmdline>root=/dev/xvda2 ro (null)</cmdline>
  </os>
  <clock offset='utc' adjustment='reset'/>
  <on_poweroff>destroy</on_poweroff>
  <on_reboot>restart</on_reboot>
  <on_crash>restart</on_crash>
  <devices>
    <disk type='block' device='disk'>
      <driver name='phy'/>
      <source dev='${vm_vol_sys_dev}'/>
      <target dev='xvda2' bus='xen'/>
    </disk>
    <disk type='block' device='disk'>
      <driver name='phy'/>
      <source dev='${vm_vol_swp_dev}'/>
      <target dev='xvda1' bus='xen'/>
    </disk>
    <interface type='bridge'>
      <mac address='${vm_mac}'/>
      <source bridge='${LIBVIRT_NET_BRIDGE}'/>
    </interface>
    <console type='pty'>
      <target type='xen' port='0'/>
    </console>
  </devices>
</domain>
EOF

virsh define /etc/libvirt/libxl/${vm_name}.xml --validate
