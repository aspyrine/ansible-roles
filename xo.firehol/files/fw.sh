#!/bin/bash
set -u

ipt4=/sbin/iptables
ipt6=/sbin/ip6tables
ipt4_tables=/proc/net/ip_tables_names
ipt6_tables=/proc/net/ip6_tables_names

_B="\033[1m"
_U="\033[4m"
_N="\033[0m"


ipt_show() {
    version=$1
    table=$2
    cmd="ipt${version}"

    ${!cmd} -nvL -t ${table} |\
        sed -E '/^(Chain)(.+)(\(.*)$/s//\1\x1b[94;1m\2\x1b[0m\3/' |\
        sed -E 's/^\s*(num|pkts).*/\x1b[33m&\x1b[0m/' |\
        sed -E '/(.+ )((REJECT|DROP))/s//\1\x1b[91m\3\x1b[0m/' |\
        sed -E '/(.+[ ,])(NEW)/s//\1\x1b[38;5;213m\2\x1b[0m/' |\
        sed -E '/(.+ )(ACCEPT)/s//\1\x1b[92m\2\x1b[0m/' |\
        sed -E '/([ds]pt[s]?:|dports )([[:digit:],]+(:[[:digit:]]+)?)/s//\1\x1b[38;5;214m\2\x1b[0m/g' |\
        sed -E '/[1-9][[:digit:]]{0,2}\.([[:digit:]]{1,3}\.){2}[[:digit:]]{1,3}(\/([[:digit:]]){1,3}){0,1}/s//\x1b[96m&\x1b[0m/g' |\
        sed -E '/(.+ )(LOGDROP)/s//\1\x1b[33m\2\x1b[0m/'|\
        sed -E 's/ (LOG|NFLOG) /\x1b[93m&\x1b[0m/'|\
        sed -E 's/\/\*.+\*\//\x1b[1m&\x1b[0m/'
}

show() {
    for i in 4 6; do
        table_file="ipt${i}_tables"
        tables=$(cat ${!table_file})

        echo -e "${_B}${_U}>>> IPv${i}${_N}"
        for table in ${tables}; do
            echo
            echo -e "${_B}>> ${table}${_N}"
            ipt_show ${i} ${table}
        done
        echo
    done
}

show
