# Anti-DDOS protection and geoip filtering

This role installs files to enable anti-ddos protect and geoip filtering
on nginx.


## Geoip Filtering

To enable, you must set `nginx_country_list` to a path for a geoip database.
And the set some countries you want to block in `nginx_blocked_countries`.

In any vhost, you can add:

```
include inc.d/blocked-countries.conf;
```

## Anti-DDOS protection

You must set `nginx_anti_ddos_secret` to any value and then, in a vhost you want to
protect, add:

```
include inc.d/anti-ddos.conf;

# optional parameters:

# cookie domain
set $anti_ddos_cookie_domain "a-domain.tld";

# change the secret
set $anti_ddos_secret "...";

# override default status
set $anti_ddos_status "500";

# change default expire time (in seconds)
set $anti_ddos_expire_time "3600";
```
