#!/usr/bin/env bash

[ -z "${HOOK_CHECK_NS}" ] && HOOK_CHECK_NS="4.2.2.2"
[ -z "${HOOK_CHECK_INTERVAL}" ] && HOOK_CHECK_INTERVAL=30
[ -z "${HOOK_CHECK_ATTEMPTS}" ] && HOOK_CHECK_ATTEMPTS=10

_log() {
    echo >&2 "   + ${@}"
}


_get_domain_name() {
    # Returns a domain from a fqdn
    local FQDN="${1}"
    /usr/bin/psl \
        --load-psl-file /usr/share/publicsuffix/public_suffix_list.dat \
        --print-reg-domain \
        "${FQDN}" \
    | cut -d" " -f2
}


_checkdns() {
    local ATTEMPT="${1}" FQDN="${2}"
    shift 2

    if [ $ATTEMPT -gt $HOOK_CHECK_ATTEMPTS ]; then
        _log "Propagation check failed after ${HOOK_CHECK_ATTEMPTS} attempts. Bailing out!"
        exit 2
    fi

    _log "Checking for dns propagation via public recursor (${HOOK_CHECK_NS})... (${ATTEMPT}/${HOOK_CHECK_ATTEMPTS})"

    records=$(host -t txt _acme-challenge.${FQDN} ${HOOK_CHECK_NS})
    local ret=0
    for TOKEN_VALUE in $@; do
        echo "${records}" | grep -- "\"${TOKEN_VALUE}\"" > /dev/null 2>&1
        ret=${?}
        if [ ${?} -ne 0 ]; then
            break
        fi
    done

    if [ ${ret} -eq 0 ]; then
        _log "Propagation success!"
        return
    else
        _log "Waiting ${HOOK_CHECK_INTERVAL}s..."
        sleep ${HOOK_CHECK_INTERVAL}
        _checkdns $((ATTEMPT+1)) ${FQDN} ${@}
    fi
}


_http_call() {
    HTTP_RC=$(curl --connect-timeout 10 --max-time 10 -L -s -w "%{http_code}" -o /dev/null "$@")

    if (( $HTTP_RC / 100 == 2)); then
        return
    else
        _log "Invalid response code ${HTTP_RC}"
        return 127
    fi
}
