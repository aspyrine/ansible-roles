#!/usr/bin/env bash

# letsencrypt.sh dns-01 challenge RFC2136 hook.
# Copyright (c) 2016 Tom Laermans.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Load hook configuration
. $BASEDIR/config_hooks

# Load functions
. $BASEDIR/hooks/functions.sh

# All the below settings can be set in the letsencrypt.sh configuration file as well.
# They will not be overwritten by the statements below if they already exist in that configuration.

# # NSUPDATE - Path to nsupdate binary
[ -z "${NSUPDATE}" ] && NSUPDATE="/usr/bin/nsupdate"

[ -z "${HOOK_NSUPDATE_NSKEY}" ] && HOOK_NSUPDATE_NSKEY=${BASEDIR}/nskey.pub
[ -z "${HOOK_NSUPDATE_NS_ADDR}" ] && HOOK_NSUPDATE_NS_ADDR="127.0.0.1"
[ -z "${HOOK_NSUPDATE_NS_PORT}" ] && HOOK_NSUPDATE_NS_PORT=53


_deploy_txt_record() {
    local FQDN="${1}"
    shift 1

    local RECORD="_acme-challenge.${FQDN}"
    for token in ${@}; do
        _log "(nsupdate) Adding ACME challenge record on ${HOOK_NSUPDATE_NS_ADDR} ${FQDN} ${token}..."
        printf "server %s %s\nupdate add %s. %d in TXT \"%s\"\n\n" \
               "${HOOK_NSUPDATE_NS_ADDR}" "${HOOK_NSUPDATE_NS_PORT}" "${RECORD}" 300 "${token}" \
               | ${NSUPDATE} -t 10 -k "${HOOK_NSUPDATE_NSKEY}"

        if [ "$?" -ne 0 ]; then
            _log "Failure reported by nsupdate. Bailing out!"
        exit 0
        fi
    done

    sleep 5
    _checkdns 1 ${FQDN} ${@}
}

_clean_txt_record() {
    local FQDN="${1}"
    local RECORD="_acme-challenge.${FQDN}"

    _log "(nsupdate) Removing ACME challenge record on ${HOOK_NSUPDATE_NS_ADDR} ${RECORD}"
    printf "server %s %s\nupdate delete %s. %d in TXT\n\n" \
    "${HOOK_NSUPDATE_NS_ADDR}" "${HOOK_NSUPDATE_NS_PORT}" "${RECORD}" 300 \
    | ${NSUPDATE} -t 10 -k "${HOOK_NSUPDATE_NSKEY}"
    if [ "$?" -ne 0 ]; then
        _log "Failure reported by nsupdate. Bailing out!"
        exit 2
    fi
}

deploy_challenge() {
    declare -A DOMAINS

    while [ $# -ne 0 ]; do
        local FQDN="${1}"
        local TOKEN_FILENAME="${2}"
        local TOKEN_VALUE="${3}"
        shift 3

        if [ -z ${DOMAINS[$FQDN]} ]; then
            DOMAINS[${FQDN}]=${TOKEN_VALUE}
        else
            DOMAINS[${FQDN}]+=" ${TOKEN_VALUE}"
        fi
    done

    for k in ${!DOMAINS[@]}; do
        _deploy_txt_record "${k}" ${DOMAINS[$k]}
    done
}

clean_challenge() {
    declare -A DOMAINS

    while [ $# -ne 0 ]; do
        local FQDN="${1}"
        local TOKEN_FILENAME="${2}"
        local TOKEN_VALUE="${3}"
        shift 3

        DOMAINS[${FQDN}]=1
    done

    for d in ${!DOMAINS[@]}; do
        _clean_txt_record ${d}
    done
}

# Call challenge hooks
HANDLER="$1"; shift
if [[ "${HANDLER}" =~ ^(deploy_challenge|clean_challenge)$ ]]; then
    "$HANDLER" "$@"
fi

# Call triggers
"${BASEDIR}/hooks/triggers.sh" "$HANDLER" "$@"
