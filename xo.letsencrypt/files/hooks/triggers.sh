#!/usr/bin/env bash
. ${BASEDIR}/hooks/functions.sh


deploy_cert() {
  local FQDN="${1}" KEYFILE="${2}" CERTFILE="${3}" CHAINFILE="${4}"
  local TRIGGER_FILE="${CERTDIR}/${alias}/trigger"

  if [ ! -x ${TRIGGER_FILE} ]; then
    _log "No trigger found"
  fi

  _log "Running trigger for ${FQDN}"

  (/bin/bash -x ${TRIGGER_FILE} 2>&1 | sed 's/^/     /') && _log "ok" || _log "failed ($?)"
}


HANDLER="$1"; shift
if [[ "${HANDLER}" =~ ^(deploy_cert)$ ]]; then
  "$HANDLER" "$@"
fi
