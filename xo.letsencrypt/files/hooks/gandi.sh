#!/usr/bin/env bash

# letsencrypt.sh dns-01 challenge RFC2136 hook using Gandi livedns
# Copyright (c) 2018 Olivier Meunier.
# Based on Tom Laermans" nsupdate hook
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Load hook configuration
. $BASEDIR/config_hooks

# Load functions
. $BASEDIR/hooks/functions.sh


# All the below settings can be set in the letsencrypt.sh configuration file as well.
# They will not be overwritten by the statements below if they already exist in that configuration.
[ -z "${HOOK_GANDI_API_KEY}" ] && HOOK_GANDI_API_KEY=""
[ -z "${HOOK_GANDI_API_URL}" ] && HOOK_GANDI_API_URL="https://api.gandi.net/v5/livedns/"

if [ "${HOOK_GANDI_API_KEY}" == "" ]; then
    _log "Gandi API key is missing"
    exit 1
fi


_get_record_name() {
    local DOMAIN="${1}" FQDN="${2}"

    HOSTNAME=${FQDN%$DOMAIN}
    if [[ "$HOSTNAME" != "" ]]; then
        echo "_acme-challenge.${HOSTNAME::-1}"
    else
        echo "_acme-challenge"
    fi
}


_deploy_txt_record() {
    local FQDN="${1}"
    shift 1

    DOMAIN=$(_get_domain_name ${FQDN})
    RECORD=$(_get_record_name ${DOMAIN} ${FQDN})

    VALUES="[\"$(echo ${@} | sed "s/ /\",\"/g")\"]"

    _log "(Gandi DNS) Adding ACME challenge record ${RECORD}.${DOMAIN} ${VALUES}..."

    _http_call --request PUT \
        --url ${HOOK_GANDI_API_URL}/domains/${DOMAIN}/records/${RECORD}/TXT \
        --header "content-type: application/json" \
        --header "Authorization: Apikey ${HOOK_GANDI_API_KEY}" \
        --data "{
            \"rrset_ttl\": 300,
            \"rrset_values\": ${VALUES}
        }"

    if (( $? != 0 )); then
        _log "Invalid HTTP response"
        return 1
    fi

    sleep 5
    _checkdns 1 ${FQDN} ${@}
}

_clean_txt_record() {
    local FQDN="${1}"
    DOMAIN=$(_get_domain_name ${FQDN})
    RECORD=$(_get_record_name ${DOMAIN} ${FQDN})

    _log "(Gandi DNS) Removing ACME challenge record ${RECORD}.${DOMAIN}..."
    _http_call --request DELETE \
        --url ${HOOK_GANDI_API_URL}domains/${DOMAIN}/records/${RECORD}/TXT \
        --header "Authorization: Apikey ${HOOK_GANDI_API_KEY}"
}

deploy_challenge() {
    declare -A DOMAINS

    while [ $# -ne 0 ]; do
        local FQDN="${1}"
        local TOKEN_FILENAME="${2}"
        local TOKEN_VALUE="${3}"
        shift 3

        if [ -z ${DOMAINS[$FQDN]} ]; then
            DOMAINS[${FQDN}]="${TOKEN_VALUE}"
        else
            DOMAINS[${FQDN}]+=" ${TOKEN_VALUE}"
        fi
    done

    for k in ${!DOMAINS[@]}; do
        _deploy_txt_record "${k}" ${DOMAINS[$k]}
    done
}

clean_challenge() {
    declare -A DOMAINS

    while [ $# -ne 0 ]; do
        local FQDN="${1}"
        local TOKEN_FILENAME="${2}"
        local TOKEN_VALUE="${3}"
        shift 3

        DOMAINS[${FQDN}]=1
    done

    for d in ${!DOMAINS[@]}; do
        _clean_txt_record ${d}
    done
}

# Call challenge hooks
HANDLER="$1"; shift
if [[ "${HANDLER}" =~ ^(deploy_challenge|clean_challenge)$ ]]; then
    "$HANDLER" "$@"
fi

# Call triggers
"${BASEDIR}/hooks/triggers.sh" "$HANDLER" "$@"
