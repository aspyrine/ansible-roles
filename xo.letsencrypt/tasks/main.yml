---

- when: letsencrypt_domains is defined and letsencrypt_domains is not none
  tags:
    - letsencrypt
  block:
    - name: Install required packages
      apt:
        name: "{{ ['dnsutils'] + letsencrypt_extra_packages }}"

    - name: Create mandatory directories
      file:
        path: '{{ item }}'
        state: directory
      with_items:
        - /var/www/letsencrypt
        - /etc/ssl/LE/hooks

    - name: Push script
      copy:
        src: dehydrated
        dest: /usr/local/bin/dehydrated
        mode: 0750

    - name: Push root chains
      copy:
        src: '{{ item }}'
        dest: '/etc/ssl/LE/{{ item }}'
        mode: 0644
      with_items:
        - chain-r3.pem

    - name: Push nsupdate keys
      copy:
        content: '{{ item.contents }}'
        dest: '/etc/ssl/LE/{{ item.name }}'
        mode: 0600
      with_items:
        - '{{ letsencrypt_nsupdate_keys.pub }}'
        - '{{ letsencrypt_nsupdate_keys.priv }}'
      when: letsencrypt_nsupdate_keys|length > 0
      loop_control:
        label: '{{ item.name }}'

    - name: Push hook scripts
      copy:
        src: 'hooks/{{ item }}'
        dest: '/etc/ssl/LE/hooks/{{ item }}'
        mode: 0700
      with_items:
        - functions.sh
        - triggers.sh
        - nsupdate.sh
        - gandi.sh

    - name: Push configuration files
      template:
        src: '{{ item }}.j2'
        dest: '/{{ item }}'
        mode: 0600
      with_items:
        - etc/ssl/LE/config.sh
        - etc/ssl/LE/config_hooks
        - etc/ssl/LE/domains.txt

    - name: Create cert directories
      file:
        path: '/etc/ssl/LE/certs/{{ item.name }}'
        state: directory
        mode: 0700
      with_items:
        - name: ''
        - '{{ letsencrypt_domains }}'
      loop_control:
        label: '/etc/ssl/LE/certs/{{ item.name }}'

    - name: Push cert configuration files
      template:
        src: 'etc/ssl/LE/_cert_config.j2'
        dest: '/etc/ssl/LE/certs/{{ item.name }}/config'
        mode: 0600
      with_items: '{{ letsencrypt_domains }}'
      loop_control:
        label: '/etc/ssl/LE/{{ item.name }}/config'

    - name: Push cert trigger files
      template:
        src: 'etc/ssl/LE/_cert_trigger.j2'
        dest: '/etc/ssl/LE/certs/{{ item.name }}/trigger'
        mode: 0700
      with_items: '{{ letsencrypt_domains }}'
      loop_control:
        label: '/etc/ssl/LE/certs/{{ item.name }}/trigger'

    - name: Install crontab
      cron:
        name: letsencrypt
        cron_file: ansible_letsencrypt
        user: root
        weekday: '1'
        minute: '5'
        hour: '0'
        job: '[ -x /usr/local/bin/dehydrated ] && /usr/local/bin/dehydrated -f /etc/ssl/LE/config.sh -c'
