#!/bin/bash

ipt4=/sbin/iptables
ipt6=/sbin/ip6tables
ipt4_restore=/sbin/iptables-restore
ipt6_restore=/sbin/ip6tables-restore
ipt4_apply=/usr/sbin/iptables-apply
ipt6_apply=/usr/sbin/ip6tables-apply
ipt4_tables=/proc/net/ip_tables_names
ipt6_tables=/proc/net/ip6_tables_names


source /etc/fw/config

if [ "${NFTABLES_COMPAT}" == "1" ]; then
    ipt4_tables="raw filter nat mangle security"
    ipt6_tables="raw filter nat mangle security"
else
    ipt4_tables=$(cat ${ipt4_tables})
    ipt6_tables=$(cat ${ipt6_tables})
fi

_B="\033[1m"
_U="\033[4m"
_N="\033[0m"


ipt_show() {
    version=$1
    table=$2
    cmd="ipt${version}"

    ${!cmd} -nvL -t ${table} |\
        sed -E '/^(Chain)(.+)(\(.*)$/s//\1\x1b[94;1m\2\x1b[0m\3/' |\
        sed -E 's/^\s*(num|pkts).*/\x1b[33m&\x1b[0m/' |\
        sed -E '/(.+ )((REJECT|DROP))/s//\1\x1b[91m\3\x1b[0m/' |\
        sed -E '/(.+ )(ACCEPT)/s//\1\x1b[92m\2\x1b[0m/' |\
        sed -E '/([ds]pt[s]?:|dports )([[:digit:],]+(:[[:digit:]]+)?)/s//\1\x1b[33;1m\2\x1b[0m/' |\
        sed -E '/[1-9][[:digit:]]{0,2}\.([[:digit:]]{1,3}\.){2}[[:digit:]]{1,3}(\/([[:digit:]]){1,3}){0,1}/s//\x1b[96m&\x1b[0m/g' |\
        sed -E '/(.+ )(LOGDROP)/s//\1\x1b[33m\2\x1b[0m/'|\
        sed -E 's/ (LOG|NFLOG) /\x1b[93m&\x1b[0m/'|\
        sed -E 's/\/\*.+\*\//\x1b[1m&\x1b[0m/'
}

ipt_flush() {
    version=$1
    tables=$2
    cmd="ipt${version}_restore"

    restore=""
    for table in $tables; do
        restore+="*${table}\n"
        case "$table" in
            "security")
                restore+=":INPUT ACCEPT\n"
                restore+=":FORWARD ACCEPT\n"
                restore+=":OUTPUT ACCEPT\n"
                ;;
            "raw")
                restore+=":PREROUTING ACCEPT\n"
                restore+=":OUTPUT ACCEPT\n"
                ;;
            "nat")
                restore+=":PREROUTING ACCEPT\n"
                restore+=":INPUT ACCEPT\n"
                restore+=":OUTPUT ACCEPT\n"
                restore+=":POSTROUTING ACCEPT\n"
                ;;
            "mangle")
                restore+=":PREROUTING ACCEPT\n"
                restore+=":INPUT ACCEPT\n"
                restore+=":FORWARD ACCEPT\n"
                restore+=":OUTPUT ACCEPT\n"
                restore+=":POSTROUTING ACCEPT\n"
                ;;
            "filter")
                restore+=":INPUT ACCEPT\n"
                restore+=":FORWARD ACCEPT\n"
                restore+=":OUTPUT ACCEPT\n"
                ;;
        esac
        restore+="COMMIT\n\n"
    done

    echo -e $restore | ${!cmd}
}

start() {
    cmd="ipt${1}_restore"
    ${!cmd} < /etc/fw/rules_ip${1}
    return $?
}

apply() {
    $ipt4_apply -w /etc/fw/rules_ip4.tmp /etc/fw/rules_ip4
    /bin/rm -f /etc/fw/rules_ip4.tmp
    $ipt6_apply -w /etc/fw/rules_ip6.tmp /etc/fw/rules_ip6
    /bin/rm -f /etc/fw/rules_ip6.tmp
}

flush() {
    for i in 4 6; do
        tables="ipt${i}_tables"
        ipt_flush $i "${!tables}"
    done
}

show() {
    for i in 4 6; do
        tables="ipt${i}_tables"

        echo -e "${_B}${_U}>>> IPv${i}${_N}"
        for table in ${!tables}; do
            echo
            echo -e "${_B}>> ${table}${_N}"
            ipt_show ${i} ${table}
        done
        echo
    done
}


case "$1" in
    start)
        if [ "${PRE_COMMANDS}" != "" ]; then
            eval ${PRE_COMMANDS}
        fi
        start 4 || exit $?
        start 6 || exit $?
        if [ "${POST_COMMANDS}" != "" ]; then
            eval ${POST_COMMANDS}
        fi
        ;;
    apply)
        apply
        ;;
    flush)
        flush
        ;;
    save)
        ;;
    show)
        show
        ;;
    *)
        echo "Usage $0 {start|apply|flush|show}"
        exit 1
esac

exit 0
